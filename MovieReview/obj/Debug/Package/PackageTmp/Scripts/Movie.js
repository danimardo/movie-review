﻿$(function () { 
    var MovieFormSubmit = function () { 
        //Grab the refernce of the form
        var $form = $(this); 
        //Build the options object
        var options = { 
            url: $form.attr("action"), 
            type: $form.attr("method"), 
            data: $form.serialize() 
        }; 
        $.ajax(options).done(function (data) { 
            var target = $($form.attr("data-movie-target")); 
            target.replaceWith(data); 
        }); 
        //To prevent the browser from doing it's defualt action means navigating 
       // away and redrawing the complete page
        return false; 
    }; 
    //Look for Form with the name "data-movie-ajax", then wire up the submit event.
    $("form[data-movie-ajax='true']").submit(MovieFormSubmit); 
});