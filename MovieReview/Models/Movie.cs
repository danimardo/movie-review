﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MovieReview.Models
{
    public class Movie
    {
        public int Id { get; set; }
       // [Required(ErrorMessage = "Debe ingresar un nombre")]
        [StringLength(50)]
        public string MovieName { get; set; }
        public string DirectorName { get; set; }
        public  string ReleaseYear { get; set; }
        public ICollection<MovieReviews> reviews { get; set; }
    }
}