﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MovieReview.Models
{

    public interface IMovieDb : IDisposable
    {
        IQueryable<T> Query<T>() where T : class;
    }

    public class MovieDb : DbContext, IMovieDb
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieReviews> MovieReviews { get; set; }

        public MovieDb() : base("name=DBConectionPeliculas") 
        {
        }

        IQueryable<T> IMovieDb.Query<T>()
        {
            return Set<T>();
        }
    }
}