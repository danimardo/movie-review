﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieReview.Controllers
{
    public class MovieController : Controller
    {
        // GET: Movie

        [HttpGet]
      //  [Authorize]
        public ActionResult Search(string name = "Casa")
        {
            var userinput = Server.HtmlEncode(name);
            ViewData["Message"] = userinput;
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}