﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MovieReview.Models;
using System.Data.Entity;

namespace MovieReview.Controllers
{

    public class MovieReviewsController : Controller
    {

        MovieDb _db = new MovieDb();
        //
        // GET: /MovieReviews/
        public ActionResult Index([Bind(Prefix = "id")] int movieId)
        {
            var movie = _db.Movies.Find(movieId);
            if (movie != null)
            {
                return View(movie);
            }
            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = _db.MovieReviews.Find(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit([Bind(Exclude = "ReviewerName")]MovieReviews review)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(review).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index", new { id = review.MovieId });
            }
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}       
   