namespace MovieReview.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MovieReview.Models.MovieDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;

            SetSqlGenerator("MySql.Data.MySqlClient", new MySql.Data.Entity.MySqlMigrationSqlGenerator());

        }

        protected override void Seed(MovieReview.Models.MovieDb context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Movies.AddOrUpdate(r => r.MovieName,
                new Movie { MovieName = "Avatar", DirectorName = "James Cameron", ReleaseYear = "2009" },
                new Movie { MovieName = "Titanic", DirectorName = "James Cameron", ReleaseYear = "1997" },
                new Movie
                {
                    MovieName = "Die  Another  Day",
                    DirectorName = "Lee  Tamahori",
                    ReleaseYear =
                "2002"
                },
                new Movie
                {
                    MovieName = "Godzilla",
                    DirectorName = "Gareth Edwards",
                    ReleaseYear = "2014",
                    reviews = new List<MovieReviews>{
                new  MovieReviews{ReviewerRating=5,ReviewerComments="Excellent",ReviewerName="Rahul Sahay"}

                   }
                });

        }
    }
}
