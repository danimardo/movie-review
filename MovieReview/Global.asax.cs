﻿using MovieReview.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MovieReview
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

          /*  if (!WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("DefaultConnection",
                "UserProfile", "UserId", "UserName", autoCreateTables: true);
            } */

            AreaRegistration.RegisterAllAreas();
            ViewEngines.Engines.RemoveAt(0); // Elimina webforms en la búsqueda de vistas
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

           /* var migrator = new DbMigrator(new Configuration());
            migrator.Update(); */


        }
    }
}
