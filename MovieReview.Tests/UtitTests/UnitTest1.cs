﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieReview.Models;
using System.Linq;
using MovieReview.Controllers;
using System.Web.Mvc;
using System.Web;
using System.Collections.Specialized;

namespace MovieReview.Tests.UtitTests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    /// 

    class FakeController : ControllerContext
    {
        private HttpContextBase _ctx = new FakeHttpContext();
        public override System.Web.HttpContextBase HttpContext
        { 
                get
                            { 
                return _ctx; 
                            }
                    set
                            { 
                                _ctx = value; 
                            } 
        } 
    }
    class FakeHttpContext : HttpContextBase
    {
        HttpRequestBase _request = new FakeHttpRequest();
        public override HttpRequestBase Request
        {
            get
            {
                return _request;
            }
        }
    }
    class FakeHttpRequest : HttpRequestBase
    {
        public override string this[string key]
        {
            get
            {
                return null;
            }
        }
        public override NameValueCollection Headers
        {
            get
            {
                return new NameValueCollection();
            }
        }
    }


    [TestClass]
    public class UnitTest1
    {
        public UnitTest1()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {

            // Organise
            var data = new Movie();
            data.reviews = new List<MovieReviews>();
            data.reviews.Add(new MovieReviews() { ReviewerRating = 5 });
            //Act
            var rater = new RateMovie(data);
            var result = rater.EvaluateRating(5);
            //Assert
            Assert.AreEqual(5, result.Rating);

        }

       
            [TestMethod]
            public void Index()
            {
                var db = new FakeMovieDb();
                db.AddSet(TestData.Movies);
                // Arrange
                HomeController controller = new HomeController(db);
                controller.ControllerContext = new FakeController();
            // Act
            ViewResult result = controller.Index() as ViewResult;
            // Assert
            Assert.IsNotNull(result.Model);
        }

            [TestMethod]
        public void Evaluate_Result_For_One_Review()
        {
            // Organise
            var data = SetupMovieAndReviews(ratings: new[] { 5 });
            //Act
            var rater = new RateMovie(data);
            var result = rater.EvaluateRating(5);
            //Assert
            Assert.AreEqual(5, result.Rating);
        }
        [TestMethod]
        public void Evaluate_Result_For_Multiple_Reviews()
        {
            // Organise
            var data = SetupMovieAndReviews(ratings: new[] { 3, 5 });
            //Act
            var rater = new RateMovie(data);
            var result = rater.EvaluateRating(5);
            //Assert
            Assert.AreEqual(4, result.Rating);

        }
        private Movie SetupMovieAndReviews(params int[] ratings)
        {
            var movie = new Movie();
            movie.reviews = new List<MovieReviews>();

            for (int i = 0; i < ratings.Length; i++)
            {
                var review = new MovieReviews()
                {
                    ReviewerRating = ratings[i],
                    MovieId = 1,
                    ReviewerComments = "Comentario",
                    ReviewerName = "Dani",
                    Id = 3 + i
                };
                if (movie.reviews != null)
                    movie.reviews.Add(review);
            }


            return movie;
        }
    }


    class TestData
    {
        public static IQueryable<Movie> Movies
        {
            get
            {
                var movies = new List<Movie>();
                for (int i = 0; i < 200; i++)
                {
                    var movie = new Movie();
                    movie.reviews = new List<MovieReviews>()
                    {
                        new MovieReviews{ReviewerRating = 5}
                    };
                }
                return movies.AsQueryable();
            }
        }
    }
}

                   